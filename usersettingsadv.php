<?php

require_once 'init.php';
require_once 'controller/FormController.php';

$skin = new Skin(ELFCHAT_ROOT . '/skin/special/default');
View::set_skin($skin);

class UserSettingsAdv extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->unauthorized_entry = false;
    }

    public function action_index()
    {
        $this->view = new View('user/usersettingsadv');
//        $this->view = new View('user/avatar');

        $form = new FormController();
        $form->AddInput(new InputCheck('remember', tr('Log in automatically?'), $this->auth->user->remember));
        $form->AddInput(new InputCheck('settings#show_tooltip', tr('Show tooltip?'), $this->auth->user->settings->show_tooltip));
        $form->AddInput(new InputCheck('settings#show_images', tr('Show images in chat?'), $this->auth->user->settings->show_images));
        $form->AddInput(new InputCheck('settings#show_smiles', tr('Show smiles in chat?'), $this->auth->user->settings->show_smiles));
        $form->AddInput(new InputCheck('settings#play_immediately', tr('Play sounds immediately in chat?'), $this->auth->user->settings->play_immediately));
        $form->SetSubmit(tr('Save'));

        if($form->Check())
        {
            $array = $form->GetArray();
            $this->auth->user->remember = $array['remember'];
            $this->auth->user->settings = $array['settings'];
            $this->auth->user->save();

            $this->view->settings = json_encode($array['settings']);
        }

        $this->view->content = $form->render();
//        $this->display();

        // --------------------------------

        $avatars = array();
        $handle = opendir(ELFCHAT_ROOT . '/avatars');
        if($handle)
        {
            while(false !== ($name = readdir($handle)))
            {
                $src = 'avatars/' . $name;
                if(is_file($src) && $name != "." && $name != "..")
                {
                    $avatars[] = $src;
                }
            }
            closedir($handle);
        }

        if(_GET('select')->is_set())
        {
            $select = _GET('select')->low();
            if($select == 'user_avatar')
            {
                $this->view->info = tr('Avatar have been uploaded.');
            }
            else
            {
                $select_number = intval($select);

                $this->auth->user->avatar = $avatars[$select_number];
                $this->auth->user->save();

                $this->view->info = tr('Avatar have been changed.');
            }

            $this->view->changed = true;
        }

        $this->view->x = Elf::Settings('avatar_size_width');
        $this->view->y = Elf::Settings('avatar_size_height');
        $this->view->user = $this->auth->user;
        $this->view->avatars = $avatars;
        $this->display();


    }

}

$userSettings = new UserSettingsAdv();
$userSettings->run();
?>
